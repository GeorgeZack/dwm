/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  	= 2;		/* border pixel of windows */
static const unsigned int snap	  		= 16;		/* snap pixel */
static const int swallowfloating		= 1;		/* 1 means swallow floating windows by default */
static const int showbar				= 1;		/* 0 means no bar */
static const int topbar			 		= 0;		/* 0 means bottom bar */
static const char *fonts[]				= { "Noto Sans Mono:style=Regular:pixelsize=11:antialias=true:autohint=true", "NotoColorEmoji:size=11:antialias=true:autohint=true" };
static const char dmenufont[]			= "monospace:size=10";
static const char col_gray1[]			= "#222222";
static const char col_gray2[]			= "#444444";
static const char col_gray3[]			= "#bbbbbb";
static const char col_gray4[]			= "#eeeeee";
static const char col_cyan[]			= "#3e5845";
static const unsigned int baralpha  	= 0xFF;
static const unsigned int borderalpha	= OPAQUE;
static const char *colors[][3] = {
	/*				fg			bg			border	*/
	[SchemeNorm] = { col_gray3,	col_gray1,	col_gray2 },
	[SchemeSel]  = { col_gray4,	col_cyan,	col_cyan  },
	[SchemeHid]  = { col_cyan,	col_gray1,	col_cyan  },
};
static const unsigned int alphas[][3] = {
	/*				fg	  		bg			border	 */
	[SchemeNorm] = { OPAQUE,	baralpha,	borderalpha },
	[SchemeSel]  = { OPAQUE,	baralpha,	borderalpha },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

/* grid of tags */
#define DRAWCLASSICTAGS			1 << 0
#define DRAWTAGGRID				1 << 1

#define SWITCHTAG_UP			1 << 0
#define SWITCHTAG_DOWN			1 << 1
#define SWITCHTAG_LEFT			1 << 2
#define SWITCHTAG_RIGHT			1 << 3
#define SWITCHTAG_TOGGLETAG		1 << 4
#define SWITCHTAG_TAG			1 << 5
#define SWITCHTAG_VIEW			1 << 6
#define SWITCHTAG_TOGGLEVIEW	1 << 7

static const unsigned int drawtagmask = DRAWTAGGRID; /* | DRAWCLASSICTAGS to show classic row of tags */
static const int tagrows = 2;

static const Rule rules[] = {
	/* xprop(1):
	*	WM_CLASS(STRING) = instance, class
	*	WM_NAME(STRING) = title
	*	WM_WINDOW_ROLE(STRING) = role
	*/
	/* class						role		instance	title							tags mask	isfloating	isterminal	noswallow	monitor */
	{ "st-256color",				NULL,		NULL,		NULL,							0,			0,		 	1,			0,			-1	},
	{ "Microsoft Teams - Preview",	NULL,		NULL,		NULL,							1 << 0,		0,		 	0,			0,			1	},
	{ NULL,							NULL,		NULL,		"Microsoft Teams Notification",	1 << 0,		1,		 	0,			0,			1	},
	{ NULL,							NULL,		NULL,		"Picture in picture",			0,			1,		 	0,			0,			-1	},
	{ "Steam",						NULL,		NULL,		NULL,							0,			1,		 	0,			0,			-1	},
	{ NULL,							NULL,		NULL,		"Friends List",					0,			1,		 	0,			0,			-1	},
	{ "mpv",						NULL,		NULL,		NULL,							0,			1,		 	0,			0,			-1	},
	{ NULL,							"pop-up",	NULL,		NULL,							0,			1,		 	0,			0,			-1	},
	{ NULL,							"alert",	NULL,		NULL,							0,			1,		 	0,			0,			-1	},
};

/* layout(s) */
static const float mfact	 = 0.5;		/* factor of master area size [0.05..0.95] */
static const int nmaster	 = 1;		/* number of clients in master area */
static const int resizehints = 0;		/* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1;	/* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol	 arrange function */
	{ "[]=",	  tile },	/* first entry is default */
	{ "><>",	  NULL },	/* no layout function means floating behavior */
	{ "[M]",	  monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,						KEY,		view,			{.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,			KEY,		toggleview,		{.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,				KEY,		tag,			{.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,		toggletag,		{.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *layoutmenu_cmd = "layoutmenu";

#include "tagall.c"
static Key keys[] = {
	/* modifier					key				function		argument */
	{ MODKEY,					XK_p,			spawn,			{.v = dmenucmd } },
	{ MODKEY|ShiftMask,			XK_Tab,			spawn,			{.v = dmenucmd } },
	{ MODKEY|ShiftMask,			XK_Return,		spawn,			{.v = termcmd } },
	{ MODKEY|ShiftMask,			XK_Caps_Lock,	spawn,			{.v = termcmd } },
	{ MODKEY,					XK_b,			togglebar,		{0} },
	{ MODKEY,					XK_j,			focusstackvis,  {.i = +1 } },
	{ MODKEY,					XK_k,			focusstackvis,  {.i = -1 } },
	{ MODKEY|ShiftMask,			XK_j,			focusstackhid,  {.i = +1 } },
	{ MODKEY|ShiftMask,			XK_k,			focusstackhid,  {.i = -1 } },
	{ MODKEY,					XK_i,			incnmaster,		{.i = +1 } },
	{ MODKEY,					XK_d,			incnmaster,		{.i = -1 } },
	{ MODKEY,					XK_h,			setmfact,		{.f = -0.05} },
	{ MODKEY,					XK_l,			setmfact,		{.f = +0.05} },
	{ MODKEY,					XK_Return,		zoom,			{0} },
	{ MODKEY,					XK_Tab,			view,			{0} },
	{ MODKEY|ShiftMask,			XK_c,			killclient,		{0} },
	{ MODKEY,					XK_t,			setlayout,		{.v = &layouts[0]} },
	{ MODKEY,					XK_f,			setlayout,		{.v = &layouts[1]} },
	{ MODKEY,					XK_m,			setlayout,		{.v = &layouts[2]} },
	{ MODKEY,					XK_space,		setlayout,		{0} },
	{ MODKEY|ShiftMask,			XK_space,		togglefloating,	{0} },
	{ MODKEY|ShiftMask,			XK_f,			togglefullscr,	{0} },
	{ MODKEY,					XK_0,			view,			{.ui = ~0 } },
	{ MODKEY|ShiftMask,			XK_0,			tag,			{.ui = ~0 } },
	{ MODKEY,					XK_comma,		focusmon,		{.i = -1 } },
	{ MODKEY,					XK_period,		focusmon,		{.i = +1 } },
	{ MODKEY|ShiftMask,			XK_comma,		tagmon,			{.i = -1 } },
	{ MODKEY|ShiftMask,			XK_period,		tagmon,			{.i = +1 } },
	{ MODKEY,					XK_s,			show,			{0} },
	{ MODKEY|ShiftMask,			XK_s,			showall,		{0} },
	{ MODKEY|ShiftMask,			XK_h,			hide,			{0} },
	TAGKEYS(					XK_1,							0)
	TAGKEYS(					XK_2,							1)
	TAGKEYS(					XK_3,							2)
	TAGKEYS(					XK_4,							3)
	TAGKEYS(					XK_5,							4)
	TAGKEYS(					XK_6,							5)
	TAGKEYS(					XK_7,							6)
	TAGKEYS(					XK_8,							7)
	TAGKEYS(					XK_9,							8)
	TAGKEYS(					XK_KP_End,						0)
	TAGKEYS(					XK_KP_Down,						1)
	TAGKEYS(					XK_KP_Page_Down,				2)
	TAGKEYS(					XK_KP_Left,						3)
	TAGKEYS(					XK_KP_Begin,					4)
	TAGKEYS(					XK_KP_Right,					5)
	TAGKEYS(					XK_KP_Home,						6)
	TAGKEYS(					XK_KP_Up,						7)
	TAGKEYS(					XK_KP_Page_Up,					8)
	TAGKEYS(					XK_KP_Insert,					9)
	{ MODKEY|ShiftMask,			XK_q,			quit,			{0} },
	{ MODKEY|Mod4Mask,			XK_Up,			switchtag,		{ .ui = SWITCHTAG_UP	 | SWITCHTAG_VIEW } },
	{ MODKEY|Mod4Mask,			XK_Down,		switchtag,		{ .ui = SWITCHTAG_DOWN	| SWITCHTAG_VIEW } },
	{ MODKEY|Mod4Mask,			XK_Right,		switchtag,		{ .ui = SWITCHTAG_RIGHT  | SWITCHTAG_VIEW } },
	{ MODKEY|Mod4Mask,			XK_Left,		switchtag,		{ .ui = SWITCHTAG_LEFT	| SWITCHTAG_VIEW } },
	{ MODKEY|ControlMask,		XK_Up,			switchtag,		{ .ui = SWITCHTAG_UP	 | SWITCHTAG_TAG | SWITCHTAG_VIEW } },
	{ MODKEY|ControlMask,		XK_Down,		switchtag,		{ .ui = SWITCHTAG_DOWN	| SWITCHTAG_TAG | SWITCHTAG_VIEW } },
	{ MODKEY|ControlMask,		XK_Right,		switchtag,		{ .ui = SWITCHTAG_RIGHT  | SWITCHTAG_TAG | SWITCHTAG_VIEW } },
	{ MODKEY|ControlMask,		XK_Left,		switchtag,		{ .ui = SWITCHTAG_LEFT	| SWITCHTAG_TAG | SWITCHTAG_VIEW } },
	{ MODKEY|ShiftMask,			XK_F1,			tagall,			{.v = "1"} },
	{ MODKEY|ShiftMask,			XK_F2,			tagall,			{.v = "2"} },
	{ MODKEY|ShiftMask,			XK_F3,			tagall,			{.v = "3"} },
	{ MODKEY|ShiftMask,			XK_F4,			tagall,			{.v = "4"} },
	{ MODKEY|ShiftMask,			XK_F5,			tagall,			{.v = "5"} },
	{ MODKEY|ShiftMask,			XK_F6,			tagall,			{.v = "6"} },
	{ MODKEY|ShiftMask,			XK_F7,			tagall,			{.v = "7"} },
	{ MODKEY|ShiftMask,			XK_F8,			tagall,			{.v = "8"} },
	{ MODKEY|ShiftMask,			XK_F9,			tagall,			{.v = "9"} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click			event mask	button		function		argument */
	{ ClkLtSymbol,		0,			Button1,	setlayout,		{0} },
	{ ClkLtSymbol,		0,			Button3,	layoutmenu,		{0} },
	{ ClkWinTitle,		0,			Button2,	zoom,			{0} },
	{ ClkStatusText,	0,			Button2,	spawn,			{.v = termcmd } },
	{ ClkWinTitle,		0,			Button1,	togglewin,		{0} },
	{ ClkClientWin,		MODKEY,		Button1,	movemouse,		{0} },
	{ ClkClientWin,		MODKEY,		Button2,	togglefloating,	{0} },
	{ ClkClientWin,		MODKEY,		Button3,	resizemouse,	{0} },
	{ ClkTagBar,		0,			Button1,	view,			{0} },
	{ ClkTagBar,		0,			Button3,	toggleview,		{0} },
	{ ClkTagBar,		MODKEY,		Button1,	tag,			{0} },
	{ ClkTagBar,		MODKEY,		Button3,	toggletag,		{0} },
};
